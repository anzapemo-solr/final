package com.edureka.zamir.solr.project.model;

import org.apache.solr.client.solrj.beans.Field;

/**
 * Hotel pojo
 * 
 * @author zamir.pena
 *
 */
public class Hotel {

	/**
	 * STATIC FIELDS OF MODEL IN SOLR'
	 */
	public final static String CITY_FIELD = "city";
	public final static String COUNTRY_FIELD = "country";
	public final static String STATE_FIELD = "state";
	public final static String UNIQUE_ID_FIELD = "id";
	public final static String FACILITIES_FIELD = "hotel_facilities";
	public final static String ROOM_FACILITIES_FIELD = "room_facilities";
	public final static String REVIEW_COUNT_FIELD = "review_count";
	public final static String REVIEW_SCORE_FIELD = "review_score";
	public final static String NAME_FIELD = "name";
	public final static String ROOM_TYPE_FIELD = "room_type";

	@Field(Hotel.CITY_FIELD)
	private String city;

	@Field(Hotel.COUNTRY_FIELD)
	private String country;

	@Field(Hotel.STATE_FIELD)
	private String state;

	@Field(Hotel.UNIQUE_ID_FIELD)
	private String uniqueId;

	@Field(Hotel.FACILITIES_FIELD)
	private String[] facilities;

	@Field(Hotel.ROOM_FACILITIES_FIELD)
	private String[] roomFacilities;

	@Field(Hotel.REVIEW_COUNT_FIELD)
	private Double reviewCount;

	@Field(Hotel.REVIEW_SCORE_FIELD)
	private Double reviewScore;
	
	@Field(Hotel.NAME_FIELD)
	private String name;
	
	@Field(Hotel.ROOM_TYPE_FIELD)
	private String roomType;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String[] getFacilities() {
		return facilities;
	}

	public void setFacilities(String[] facilities) {
		this.facilities = facilities;
	}

	public String[] getRoomFacilities() {
		return roomFacilities;
	}

	public void setRoomFacilities(String[] roomFacilities) {
		this.roomFacilities = roomFacilities;
	}

	public Double getReviewCount() {
		return reviewCount;
	}

	public void setReviewCount(Double reviewCount) {
		this.reviewCount = reviewCount;
	}

	public Double getReviewScore() {
		return reviewScore;
	}

	public void setReviewScore(Double reviewScore) {
		this.reviewScore = reviewScore;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

}
