package com.edureka.zamir.solr.project.server;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.staticFileLocation;

import com.edureka.zamir.solr.project.model.Hotel;
import com.edureka.zamir.solr.project.solr.SolrClientHelper;
import com.edureka.zamir.solr.project.util.solr.SolrToJson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Acts as the server for serving pages
 * 
 * @author zamir.pena
 *
 */
public class Server {
	
	public static final int RESULTS_PER_PAGE = 10;
	public static JsonParser parser = new JsonParser();

	/**
	 * Starts the server
	 */
	public static void start() {
		System.out.println("Starting server");
		setPaths();
		System.out.println("Server started at http://localhost:4567");
	}

	/**
	 * Set the paths
	 */
	public static void setPaths() {
		setPagePaths();
		setSearchPaths();
		setFacetingPaths();
	}

	/**
	 * Set the "/" path, for clients.
	 */
	public static void setPagePaths() {
		// For static content
		staticFileLocation("/static");

		// Redirect when "/" is requested
		get("/", (request, response) -> {
			response.redirect("/index");
			return null;
		});
	}

	/**
	 * Sets the search paths
	 */
	public static void setSearchPaths() {
		post("/search", (request, response) -> {
			try {
				JsonObject object = stringToJson(request.body());
				String country = object.get("country").getAsString();
				String city = object.get("city").getAsString();
				String name = object.get("name").getAsString();
				String roomType = object.get("roomType").getAsString();
				String roomFacility = object.get("roomFacility").getAsString();
				int page = Integer.parseInt(object.get("page").getAsString());
				return SolrToJson.queryResponseFromSearchToJsonObject(SolrClientHelper.queryHotels(name, city, country, roomType, roomFacility, page));
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error searching hotels: " + e.getLocalizedMessage());
			}
			return null;
		});
	}
	
	/**
	 * Sets the server paths for faceting
	 */
	public static void setFacetingPaths() {
		post("/faceting/room/facilities", (request, response) -> {
			try {
				return SolrToJson.listCountFacetsToJsonObject(SolrClientHelper.searchFacet(Hotel.ROOM_FACILITIES_FIELD)).toString();
			} catch (Exception e) {
				System.out.println("Error retrieving facetings:" + e.getLocalizedMessage());
			}
			return null;
		});
		
		post("/faceting/room/types", (request, response) -> {
			try {
				return SolrToJson.listCountFacetsToJsonObject(SolrClientHelper.searchFacet(Hotel.ROOM_TYPE_FIELD)).toString();
			} catch (Exception e) {
				System.out.println("Error retrieving facetings:" + e.getLocalizedMessage());
			}
			return null;
		});
		
		post("/faceting/city", (request, response) -> {
			try {
				return SolrToJson.listCountFacetsToJsonObject(SolrClientHelper.searchFacet(Hotel.CITY_FIELD)).toString();
			} catch (Exception e) {
				System.out.println("Error retrieving facetings:" + e.getLocalizedMessage());
			}
			return null;
		});
		
		post("/faceting/country", (request, response) -> {
			try {
				return SolrToJson.listCountFacetsToJsonObject(SolrClientHelper.searchFacet(Hotel.COUNTRY_FIELD)).toString();
			} catch (Exception e) {
				System.out.println("Error retrieving facetings:" + e.getLocalizedMessage());
			}
			return null;
		});
	}
	
	/**
	 * Parses a string json and return a json object
	 * @param json
	 * @return
	 */
	private static JsonObject stringToJson(String json) {
		return parser.parse(json).getAsJsonObject();
	}

}
