package com.edureka.zamir.solr.project.util.solr;

import java.util.List;

import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;

import com.edureka.zamir.solr.project.model.Hotel;
import com.edureka.zamir.solr.project.server.Server;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * Util class for passing Solr responses or objects to JSON
 * 
 * @author zamir.pena
 *
 */
public class SolrToJson {

	/**
	 * Returns a JsonObject iterating a list of counts
	 * 
	 * @param counts
	 * @return
	 */
	public static JsonObject listCountFacetsToJsonObject(
			List<org.apache.solr.client.solrj.response.FacetField.Count> counts) {
		JsonObject object = new JsonObject();

		if (counts == null || counts.size() == 0) {
			return null;
		}

		for (Count count : counts) {
			object.addProperty(count.getName(), count.getCount());
		}
		return object;
	}

	/**
	 * Converts a single search query response to an Json Object
	 * 
	 * @param queryResponse
	 * @return
	 */
	public static JsonObject queryResponseFromSearchToJsonObject(QueryResponse queryResponse) {
		JsonObject object = new JsonObject();
		object.addProperty("count", queryResponse.getResults().getNumFound());
		List<Hotel> hotels = queryResponse.getBeans(Hotel.class);
		object.addProperty("hotels", new Gson().toJson(hotels));
		object.addProperty("pages", Math.ceil(queryResponse.getResults().getNumFound() / Server.RESULTS_PER_PAGE));
		return object;
	}

}
