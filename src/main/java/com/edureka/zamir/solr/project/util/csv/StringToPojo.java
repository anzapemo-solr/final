package com.edureka.zamir.solr.project.util.csv;

import java.util.ArrayList;

import com.edureka.zamir.solr.project.model.Hotel;

/**
 * Converts ArrayList<String[]> to Pojos {@link Hotel}
 * 
 * @author zamir.pena
 *
 */
public class StringToPojo {

	public static ArrayList<Hotel> readHotels(ArrayList<String[]> csvStringData) {
		ArrayList<Hotel> hotels = new ArrayList<Hotel>();
		for (String[] hotelData : csvStringData) {
			Hotel hotel = new Hotel();
			hotel.setCity(hotelData[3]);
			hotel.setCountry(hotelData[4]);
			hotel.setFacilities(hotelData[10].split(CSVReaderHelper.MUTLI_VALUE_SEPARATOR));
			hotel.setName(hotelData[19]);
			hotel.setRoomFacilities(hotelData[27].split(CSVReaderHelper.MUTLI_VALUE_SEPARATOR));
			hotel.setRoomType(hotelData[28]);
			hotel.setState(hotelData[34]);
			String uniqueId = hotelData[35];
			if (uniqueId == null || uniqueId.length() == 0) {
				uniqueId = String.valueOf(System.currentTimeMillis());
			}
			hotel.setUniqueId(uniqueId);
			try {
				hotel.setReviewCount(Double.parseDouble(hotelData[30]));
			} catch (Exception e) {
				hotel.setReviewCount(new Double(0));
			}

			try {
				hotel.setReviewScore(Double.parseDouble(hotelData[31]));
			} catch (Exception e) {
				hotel.setReviewScore(new Double(0));
			}
			hotels.add(hotel);
		}
		return hotels;
	}

}
