package com.edureka.zamir.solr.project;

import java.util.ArrayList;

import com.edureka.zamir.solr.project.model.Hotel;
import com.edureka.zamir.solr.project.server.Server;
import com.edureka.zamir.solr.project.solr.SolrClientHelper;
import com.edureka.zamir.solr.project.util.csv.CSVReaderHelper;
import com.edureka.zamir.solr.project.util.csv.StringToPojo;
import com.edureka.zamir.solr.project.util.solr.PojoToDocument;

/**
 * Main class
 *
 */
public class App {

	public static void main(String[] args) {
//		boolean skipIndex = true;
		boolean skipIndex = false;
		/**
		 * Read the docs from CSV and index them
		 */
		if (!skipIndex) {
			try {
				System.out.println("Indexing documents");
				ArrayList<Hotel> hotels = StringToPojo.readHotels(CSVReaderHelper.readCSV("indian_hotels.csv"));
				SolrClientHelper.indexDocuments(PojoToDocument.pojoListToDocumentList(hotels));
				System.out.println("Documents indexed");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error indexing docs. Exiting...");
				System.exit(1);
			}
		}

		/**
		 * Then start the server (localhost:4567)
		 */
		Server.start();
	}
}
