package com.edureka.zamir.solr.project.solr;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds Solr' data
 * @author zamir.pena
 *
 */
public class SolrData {

	public static String HOST_URL = "http://localhost:8983/solr";
	public static String CORE_NAME = "hotel_catalog";
	/**
	 * List of zookeeper hosts
	 */
	public static List<String> ZK_HOSTS = new ArrayList<String>();
	
	static {
		ZK_HOSTS.add("localhost:9983");
	}
	
}
