package com.edureka.zamir.solr.project.util.solr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.SolrInputField;

import com.edureka.zamir.solr.project.model.Hotel;

/**
 * Converts a Pojo {@link Hotel} to {@link SolrInputDocument}
 * 
 * @author zamir.pena
 *
 */
public class PojoToDocument {

	public static SolrInputDocument pojoToDocument(Hotel hotel) {
		Map<String, SolrInputField> fields = new HashMap<String, SolrInputField>();

		fields.put(Hotel.UNIQUE_ID_FIELD, getSolrInputField(Hotel.UNIQUE_ID_FIELD, hotel.getUniqueId()));
		fields.put(Hotel.CITY_FIELD, getSolrInputField(Hotel.CITY_FIELD, hotel.getCity()));
		fields.put(Hotel.COUNTRY_FIELD, getSolrInputField(Hotel.COUNTRY_FIELD, hotel.getCountry()));
		fields.put(Hotel.FACILITIES_FIELD, getSolrInputField(Hotel.FACILITIES_FIELD, hotel.getFacilities()));
		fields.put(Hotel.NAME_FIELD, getSolrInputField(Hotel.NAME_FIELD, hotel.getName()));
		fields.put(Hotel.REVIEW_COUNT_FIELD, getSolrInputField(Hotel.REVIEW_COUNT_FIELD, hotel.getReviewCount()));
		fields.put(Hotel.REVIEW_SCORE_FIELD, getSolrInputField(Hotel.REVIEW_SCORE_FIELD, hotel.getReviewScore()));
		fields.put(Hotel.ROOM_FACILITIES_FIELD,
				getSolrInputField(Hotel.ROOM_FACILITIES_FIELD, hotel.getRoomFacilities()));
		fields.put(Hotel.ROOM_TYPE_FIELD, getSolrInputField(Hotel.ROOM_TYPE_FIELD, hotel.getRoomType()));
		fields.put(Hotel.STATE_FIELD, getSolrInputField(Hotel.STATE_FIELD, hotel.getState()));

		SolrInputDocument doc = new SolrInputDocument(fields);

		return doc;
	}

	/**
	 * Converts a list of {@link Hotel} to a list of {@link SolrInputDocument}
	 * 
	 * @param hotels
	 * @return
	 */
	public static ArrayList<SolrInputDocument> pojoListToDocumentList(ArrayList<Hotel> hotels) {
		ArrayList<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
		for (Hotel hotel : hotels) {
			docs.add(pojoToDocument(hotel));
		}
		return docs;
	}

	/**
	 * Returns a SolrInputField
	 * 
	 * @param value
	 * @return
	 */
	private static SolrInputField getSolrInputField(String name, Object value) {
		SolrInputField field = new SolrInputField(name);
		field.setValue(value);
		return field;
	}

}
