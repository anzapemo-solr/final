package com.edureka.zamir.solr.project.solr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.request.GenericSolrRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.MapSolrParams;
import org.apache.solr.common.params.SolrParams;

import com.edureka.zamir.solr.project.server.Server;

/**
 * Helper for all the operations
 * 
 * @author Zamir
 *
 */
public class SolrClientHelper {

	/**
	 * Create a SolrJ client
	 */
	private static SolrClient client;
	/**
	 * Another client for index documents
	 */
	private static SolrClient searchClient;

	/**
	 * Initialization
	 */
	static {
		initializeClients();
	}

	public static void initializeClients() {
		client = new CloudSolrClient.Builder(SolrData.ZK_HOSTS, Optional.empty()).build();
		searchClient = new CloudSolrClient.Builder(SolrData.ZK_HOSTS, Optional.empty()).build();
		((CloudSolrClient) client).setDefaultCollection(SolrData.CORE_NAME);
		((CloudSolrClient) searchClient).setDefaultCollection(SolrData.CORE_NAME);
	}

	/**
	 * Makes a request against Solr'
	 * 
	 * @param mapParams
	 *            Map of params for the request
	 * @param method
	 *            REST method
	 * @param path
	 *            URL path for client
	 * @throws Exception
	 */
	public static void makeRequest(Map<String, String> mapParams, SolrRequest.METHOD method, String path)
			throws Exception {
		SolrParams params = new MapSolrParams(mapParams);
		GenericSolrRequest req = new GenericSolrRequest(SolrRequest.METHOD.POST, path, params);
		client.request(req);
	}

	/**
	 * Adds a list of SolrInputDocument to the index
	 * 
	 * @param docs
	 * @throws Exception
	 */
	public static void indexDocuments(ArrayList<SolrInputDocument> docs) throws Exception {
		System.out.println("Indexing: " + docs.size() + " docs.");
		int counter = 0;
		for (SolrInputDocument doc : docs) {
			try {
				client.add(SolrData.CORE_NAME, doc);

				counter++;
				// Each 100 docs, commit.
				if (counter % 100 == 0) {
					client.commit(SolrData.CORE_NAME);
					System.out.println("Indexed: " + counter + " docs.");
				}
			} catch (Exception e) {
				System.out.println("Error indexing document: " + e.getLocalizedMessage());
				e.printStackTrace();
			}
		}
		client.commit(SolrData.CORE_NAME);
	}

	/**
	 * Makes a query to /select RequestHandler
	 * 
	 * @param query
	 * @return The query response
	 * @throws Exception
	 */
	public static QueryResponse query(String query, int page) throws Exception {
		SolrQuery solrQuery = new SolrQuery();
		solrQuery.setRequestHandler("/select");
		solrQuery.setQuery(query);

		if (page > 0) {
			int pageTemp = page - 1;
			int skip = pageTemp * Server.RESULTS_PER_PAGE;
			solrQuery.set("rows", Server.RESULTS_PER_PAGE);
			solrQuery.set("start", skip);
		}

		return searchClient.query(solrQuery);
	}

	/**
	 * Search for hotels given the parameters
	 * 
	 * @param name
	 * @param city
	 * @param country
	 * @param roomType
	 * @return
	 * @throws Exception
	 */
	public static QueryResponse queryHotels(String name, String city, String country, String roomType, String roomFacility, int page)
			throws Exception {
		String query = "";
		boolean queryChanged = false;

		if (checkParam(name)) {
			queryChanged = true;
			query = "name:\"" + name + "\"~10";
		}

		if (checkParam(city)) {
			String part = "city:" + city;
			if (queryChanged) {
				query += " AND " + part;
			} else {
				query = part;
			}
			queryChanged = true;
		}
		
		if (checkParam(country)) {
			String part = "country:" + country;
			if (queryChanged) {
				query += " AND " + part;
			} else {
				query = part;
			}
			queryChanged = true;
		}
		
		if (checkParam(roomType)) {
			String part = "room_type:" + roomType;
			if (queryChanged) {
				query += " AND " + part;
			} else {
				query = part;
			}
			queryChanged = true;
		}
		
		if (checkParam(roomFacility)) {
			String part = "room_facilities:" + roomFacility;
			if (queryChanged) {
				query += " AND " + part;
			} else {
				query = part;
			}
			queryChanged = true;
		}

		if (!queryChanged) {
			query = "*:*";
		}

		return query(query, page);
	}

	/**
	 * Check if a param is valid or not
	 * 
	 * @param param
	 * @return
	 */
	private static boolean checkParam(String param) {
		if (param != null) {
			if (param.length() > 0 && (!param.equals("-1"))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Search for facets on a single field
	 *
	 * @param field
	 * @return
	 * @throws Exception
	 */
	public static List<Count> searchFacet(String field) throws Exception {
		SolrQuery query = new SolrQuery();
		query.setRequestHandler("/select");
		query.set("rows", 0);
		query.setQuery("*:*");
		query.setFacet(true);
		query.addFacetField(field);

		QueryResponse qr = searchClient.query(query);
		FacetField facet = qr.getFacetField(field);

		return facet.getValues();
	}
}
