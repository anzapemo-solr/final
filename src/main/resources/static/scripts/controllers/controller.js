/**
 * Main angular controller for app 
 */

var app = angular.module("hotelApp");

app.controller("appController", ["$scope", "simpleSearchService", function($scope, simpleSearchService){
	
	$scope.fastName = "";
	$scope.name = "";
	$scope.page = 1;
	$scope.numPages = 0;
	$scope.pages = [];
	$scope.hotels = [];
	$scope.totalHotels;
	$scope.cityOptions = {};
	$scope.countryOptions = {};
	$scope.roomTypeOptions = {};
	$scope.roomFacilitiesOptions = {};
	$scope.lastQuery = {};
	
	/**
	 * Performs the search
	 */
	$scope.search = function() {
		var city = null;
		var country = null;
		var roomType = null;
		var roomFacility = null;
		
		if ($scope.cityOptions.selectedOption) {
			city = $scope.cityOptions.selectedOption.value;
		}
		
		if ($scope.countryOptions.selectedOption) {
			country = $scope.countryOptions.selectedOption.value;
		}
		
		if ($scope.roomTypeOptions.selectedOption) {
			roomType = $scope.roomTypeOptions.selectedOption.value;
		}
		
		if ($scope.roomFacilitiesOptions.selectedOption) {
			roomFacility = $scope.roomFacilitiesOptions.selectedOption.value;
		}
		
		simpleSearchService.searchHotels($scope.name, city, country, roomType, roomFacility, $scope.page, $scope.renderHotels, function(error){console.log(error)});
		$scope.lastQuery.name = $scope.name;
		$scope.lastQuery.city = city
		$scope.lastQuery.country = country
		$scope.lastQuery.roomType = roomType
		$scope.lastQuery.roomFacility = roomFacility
	}
	
	/**
	 * Performs a search only by name (upper field)
	 */
	$scope.nameSearch = function() {
		simpleSearchService.searchHotels($scope.fastName, null, null, null, null, $scope.page, $scope.renderHotels, function(error){console.log(error)});
		$scope.lastQuery.name = $scope.fastName;
		$scope.lastQuery.city = null;
		$scope.lastQuery.country = null;
		$scope.lastQuery.roomType = null;
		$scope.lastQuery.roomFacility = null;
	}
	
	
	/**
	 * Repeat the query with the specified page
	 */
	$scope.changePage = function(pageNumber) {
		$scope.page = pageNumber;
		simpleSearchService.searchHotels($scope.lastQuery.name, $scope.lastQuery.city, $scope.lastQuery.country, $scope.lastQuery.roomType, $scope.lastQuery.roomFacility, $scope.page, $scope.renderHotels, function(error){console.log(error)});
	}
	
	/**
	 * Render the hotels results and pagination
	 */
	$scope.renderHotels = function(hotelsResponse) {
		console.log(hotelsResponse);
		$scope.hotels = hotelsResponse.hotels;
		$scope.totalHotels = hotelsResponse.count;
		$scope.numPages = hotelsResponse.numPages;
		
		$scope.pages = [];
		for (var i = 0; i < $scope.numPages; i++) {
			$scope.pages.push(i+1);
		}
		
		if ($scope.totalHotels > 0) {
			$("#alertHotelsFound").fadeIn();
			$("#alertHotelsNotFound").hide();
		} else {
			$("#alertHotelsFound").hide();
			$("#alertHotelsNotFound").fadeIn();
		}
		
	}
	
	/**
	 * Initialize the values
	 * @returns
	 */
	$scope.init = function() {
		simpleSearchService.searchFacets("country", $scope.fillCountries, function(error){console.log(error)}, false);
		simpleSearchService.searchFacets("city", $scope.fillCities, function(error){console.log(error)}, false);
		simpleSearchService.searchFacets("roomTypes", $scope.fillRoomTypes, function(error){console.log(error)}, false);
		simpleSearchService.searchFacets("roomFacilities", $scope.fillRoomFacilities, function(error){console.log(error)}, false);
		$("#alertHotelsFound").hide();
		$("#alertHotelsNotFound").hide();
	}
	
	/**
	 * Fills the countries for selection
	 */
	$scope.fillCountries = function(data) {
		$scope.countryOptions.model = null;
		$scope.countryOptions.options = data;
	}
	
	/**
	 * Fills the cities for selection
	 */
	$scope.fillCities = function(data) {
		$scope.cityOptions.model = null;
		$scope.cityOptions.options = data;
	}
	
	/**
	 * Fills the room types for selection
	 */
	$scope.fillRoomTypes = function(data) {
		$scope.roomTypeOptions.model = null;
		$scope.roomTypeOptions.options = data;
	}
	
	/**
	 * Fills the room facilities for selection
	 */
	$scope.fillRoomFacilities = function(data) {
		$scope.roomFacilitiesOptions.model = null;
		$scope.roomFacilitiesOptions.options = data;
	}
	
	//Calls the init function
	$scope.init();
	
}]);