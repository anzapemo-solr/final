/**
 * Service for faceting results management
 */

var app = angular.module("hotelApp");

app.service("facetingService", function() {
	var facetingService = {};
	
	/**
	 * Formats the faceting results
	 * From -> {"text":number, "text2": number}
	 * To -> [{value: "text", count: number}, {...}, {...},]
	 */
	facetingService.formatFacetingResults = function(results) {
		var formattedResults = [];
		if (results != null) {
			for (var key in results) {
				var result = {};
				result.value = key.toUpperCase();
				result.count = results[key];
				result.show = key.toUpperCase() + " (" + results[key] + ")" ;
				formattedResults.push(result);
			}
			
			return formattedResults;
		} else {
			return null
		}
	}
	
	/**
	 * Formats the faceting results to a simpler form
	 */
	facetingService.simpleFormatFacetingResults = function(results) {
		var formattedResults = [];
		if (results != null) {
			for (var key in results) {
				formattedResults.push(key.toUpperCase() + " (" + results[key] + ")" );
			}
			return formattedResults;
		} else {
			return null
		}
	}
	
	return facetingService;
});