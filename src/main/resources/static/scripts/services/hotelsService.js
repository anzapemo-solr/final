/**
 * Service for formatting hotel results
 */

var app = angular.module("hotelApp");

app.service("hotelsService", [function(){
	
	var hotelsService = {};
	
	/**
	 * Parse the $http received results
	 */
	hotelsService.parseResults = function(results) {
		if (results.data) {
			var dataTemp = {};
			dataTemp.count = results.data.count;
			dataTemp.hotels = JSON.parse(results.data.hotels);
			dataTemp.numPages = results.data.pages;
			results.data = null;
			return dataTemp;
		}
		return null;
	}
	
	return hotelsService;
}]);