/**
 * Simple search service
 */

var app = angular.module("hotelApp");

app.service("simpleSearchService", ["$http", "facetingService", "hotelsService",  function($http, facetingService, hotelsService) {
	
	var simpleSearchService = {};
	
	simpleSearchService.search = "/search";
	simpleSearchService.facetingRoomFacilities = "/faceting/room/facilities";
	simpleSearchService.facetingRoomTypes = "/faceting/room/types";
	simpleSearchService.facetingCity = "/faceting/city";
	simpleSearchService.facetingCountry = "/faceting/country";
	
	/**
	 * Validate a param
	 * If it's null, void or "", passes param to the desired value
	 */
	simpleSearchService.validateParam = function(param, value) {
		if (param == null || param.length == 0 || param == "") {
			return value;
		}
		return param;
	}
	
	/**
	 * Search for hotels
	 */
	simpleSearchService.searchHotels = function(name, city, country, roomType, roomFacility, page, success, error) {
		name = simpleSearchService.validateParam(name, -1);
		city = simpleSearchService.validateParam(city, -1);
		country = simpleSearchService.validateParam(country, -1);
		roomType = simpleSearchService.validateParam(roomType, -1);
		roomFacility = simpleSearchService.validateParam(roomFacility, -1);
		
		reqData = {};
		reqData.name = name;
		reqData.city = city;
		reqData.country = country;
		reqData.roomType = roomType;
		reqData.roomFacility = roomFacility;
		reqData.page = page;
		
		$http({
			method: "POST",
			url: simpleSearchService.search,
			data: reqData
		})
		.then(function onSuccess(response){
			success(hotelsService.parseResults(response));
		})
		.catch(function onError(response){
			error(response)
		});
	}
	
	/**
	 * Search for facets
	 */
	simpleSearchService.searchFacets = function(type, success, error, simple) {
		var urlTarget = "";
		
		switch (type) {
		case "city":
			urlTarget = simpleSearchService.facetingCity;
			break;
		
		case "country":
			urlTarget = simpleSearchService.facetingCountry;
			break;
			
		case "roomTypes":
			urlTarget = simpleSearchService.facetingRoomTypes;
			break;
			
		case "roomFacilities":
			urlTarget = simpleSearchService.facetingRoomFacilities;
			break;

		default:
			error();
			break;
		}
		
		return $http({
			method: "POST",
			url: urlTarget
		})
		.then(function onSuccess(response){
			var data = response.data;
			if (data != null) {
				if (simple) {
					data = facetingService.simpleFormatFacetingResults(data);
				} else {
					data = facetingService.formatFacetingResults(data);
				}
				
				success(data);
			} else {
				error(response);
			}
			
		})
		.catch(function onError(response){
			error(response);
		});
		
	}
	
	return simpleSearchService;
	
}]);