/**
 * Directive for hotel
 */

var app = angular.module("hotelApp");

app.directive("hotel", function(){
	return {
		restrict: 'E',
		scope: {
			hotel : '='
		},
		templateUrl: '/templates/hotelTemplate.html'
	}
});